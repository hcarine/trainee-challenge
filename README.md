# Trainee Challenge: HTML/CSS #

Esse desafio testará as principais habilidades que um desenvolvedor front-end deve possuir para transformar um mockup (desenho ainda não funcional da tela) em um protótipo.
Veja abaixo, os passos que você deve executar para concluir o desafio.

### Instruções ###

- Forke esse repositório e faça seu desafio numa branch chamada ```seunome_ano-mes-dia``` (use dois dígitos para as informações numéricas).
- Seu objetivo principal é transformar esse [mockup](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e0ff668aa6e6e6bdaeb7f87b7119e/mockup/mockup.png?at=master) em um protótipo funcional HTML/CSS da maneira mais idêntica possível (pixel-perfect).
- Seu HTML deverá ser o mais semântico possível (título devem usar H1, H2, ..., parágrafos deve usar P, formulários devem estar dentro de uma tag FORM, e assim por diante).
- Seu CSS não deverá ser inline (no arquivo HTML não deverá conter qualquer tipo de estilo). Todos os estilos devem estar num arquivo CSS externos ao HTML.
- A barra azul contendo o logo do ContaAzul (top-bar) deve ser fixa (não rolar com a página).
- As pseudo-classes ```hover```, ```focus``` e ```active``` nos botões devem deixá-los com 75% de sua opacidade.
- Crie um diretório chamado ```prototype```. Todos os arquivos que você criar devem estar nesse diretório. Se achar necessário, subdivida o diretório da maneira que achar melhor.
- Assim que concluído o desafio, abra um pull request com suas alterações e envie um email para [domphysis@contaazul.com](mailto:domphysis@contaazul.com) entitulado ```Trainee Challenge: HTML/CSS``` contendo o link para o pull request.

### Dicas ###

- No diretório [guidelines](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e0ff668aa6e6e6bdaeb7f87b7119e/guidelines/?at=master) você encontra o código hexadecimal de todas as cores usadas no mockup.
- No diretório [mockup](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e/mockup/?at=master) você encontra a imagem [mockup-grided](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e0ff668aa6e6e6bdaeb7f87b7119e/mockup/mockup_grided.png?at=master). Essa imagem exibe um grid onde cada quadrado equivale a 10 pixels (horizontal/vertical) facilitando assim a definição de ```margin``` e ```padding```.
- No diretório [images](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e0ff668aa6e6e6bdaeb7f87b7119e/images/?at=master) você encontra o logo do ContaAzul usado no mockup.
- No diretório [webfonts](https://bitbucket.org/rafaelcamargo/trainee-challenge/src/0fd5bf4c336e0ff668aa6e6e6bdaeb7f87b7119e/webfonts/?at=master) você encontra as fonts usadas no mockup.
- Jamais use ```!important``` nos estilos.
- Em caso de dúvida, envie um email para [domphysis@contaazul.com](mailto:domphysis@contaazul.com) ou peça um help para o seu Papai Smurf


Boa sorte!